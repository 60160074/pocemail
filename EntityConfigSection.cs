﻿using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    internal class EntityConfigSection
    {
        public static readonly string ConnectionStringName = "Demo";

        public static string ConnectionString
        {
            get { return ConnectionStringConfigManager.GetConnectionString("Demo"); }
        }
    }
}
