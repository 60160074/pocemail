﻿using System.Collections.Generic;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;
using System.Collections;

namespace  MonitorApp
{
    public interface IMoniterService
    {
        
        List<Moniter> GetList(MoniterFilter filter, ResultPaging paging);
        List<Moniter> Moniter(List<Moniter> list,Moniter moniter);
        Moniter GetData(int moniterId);
        Moniter SaveData(Moniter moniter);
        List<Moniter> GetListLog(MoniterFilter filter, ResultPaging paging,ListMoniterModel model);
        bool DeleteData(int moniter);
    }
}
