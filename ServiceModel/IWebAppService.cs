﻿using System.Collections.Generic;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;
using System.Collections;
using System.Threading;

namespace  MonitorApp
{
    public interface IWebAppservice
    {

        List<WebApp> GetList(WebAppFilter filter, ResultPaging paging);
        WebApp GetData(int moniterId);
        WebApp SaveData(WebApp moniter);
        bool DeleteData(int moniter);
        List<WebApp> Moniter(List<WebApp> list);
    }
}