﻿using  MonitorApp;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class BaseController : Controller
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(BaseController));
        private DemoConfigSection _appConfigSection = null;

        /*
         * Session data to support unit testing.
         */
        protected internal AppSession UnitTestSession { get; set; }
        protected AppSession AppSession
        {
            get
            {
                if (HttpContext.Session == null)
                    return UnitTestSession;
                return AppSession.CreateInstance(HttpContext.Session);
            }
            set
            {
                if (HttpContext.Session == null)
                    UnitTestSession = value;
                else
                {
                    HttpContext.Session.SetString(AppSession.SessionName, JsonConvert.SerializeObject(value));
                    if (value == null)
                        HttpContext.Session.Remove(AppSession.SessionName);
                }
            }
        }

        protected DemoConfigSection AppConfigSection
        {
            get
            {
                if (_appConfigSection == null)
                    _appConfigSection = DemoConfigSection.GetInstance();
                return _appConfigSection;
            }
        }
    }
}
