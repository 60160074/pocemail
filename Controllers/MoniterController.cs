﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;
using System.Net.NetworkInformation;
using System.Net;
using TwoMind.Infrastructure.Entity.Sql;
using System.Linq;
using System.Data.SqlClient;
using System.Data;

namespace MonitorApp
{
    public class MoniterController : BaseController
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(MoniterController));

        public static readonly string Name = "Moniter";
        public static readonly string ActionLog = "MoniterLog";
        public static readonly string ActionList = "List";
        public static readonly string ActionEdit = "Edit";
        public static readonly string ActionAdd = "Add";
        public static readonly string ActionDelelete = "Delete";
        // /moniterwebapp
        // /moniterwebapp/index
        public IActionResult index()
        {
            return RedirectToAction(MoniterController.ActionList);
        }
        public IActionResult MoniterLog(ListMoniterModel model,int pageNo = 1)
        {
            const string func = "MoniterLog";
            try
            {
                IMoniterService moniterservice = new MoniterService(new MoniterRepository());
                ResultPaging paging = new ResultPaging(UCHelper.PagePerScreen, pageNo);
              
               
                List<Moniter> lomoniter = moniterservice.GetListLog(model.ToMonitorFilter(), paging, model);
                model.Moniters.AddRange(MoniterModel.CreateModels(lomoniter));
                model.Paging.ResultPaging = paging;
                model.FillLookupList();





                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("{0}: Exception caught.", func, ex);
                throw ex;
            }
          
        }


        // /moniterwebapp/list


        public IActionResult List(ListMoniterModel model, ListWebAppModel model2,int pageNo = 1)
        {
            const string func = "List";
            try
            {
                IMoniterService moniterservice = new MoniterService(new MoniterRepository());
                ResultPaging paging = new ResultPaging(UCHelper.PagePerScreen, pageNo);
                List<Moniter> lmoniter = moniterservice.GetList(model.ToMonitorFilter(), paging);
                
                model.Moniters.AddRange(MoniterModel.CreateModels(lmoniter));
                model.Paging.ResultPaging = paging;
                model.FillLookupList();





                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("{0}: Exception caught.", func, ex);
                 throw ex;
            }
        }
        public IActionResult Edit(int? id,bool status)

        {
            var a = id;
            if(status  == true){
                ViewBag.status = "add";
            }
            else
            {
                ViewBag.status = "edit";
            }
            const string func = "Edit";

            try
            {
                EditMoniterModel model = new EditMoniterModel();
                if (id.HasValue)
                {
                    IMoniterService service = new MoniterService(new MoniterRepository());
                    Moniter moniterwebapp = service.GetData(id.Value);
                    if (moniterwebapp == null)
                        return RedirectToAction(MoniterController.ActionList);
                    model = new EditMoniterModel(moniterwebapp);
                }
                else
                    model = new EditMoniterModel();
                model.FillLookupList();


                return View(model);
            }
            catch (Exception ex)
            {
                _logger.Error("{0}: Exception caught with id {1}.", func, id, ex);
                throw ex;
            }
        }





        [HttpPost]
        public IActionResult Edit(EditMoniterModel model)//รับค่าโมเดล

        {


            WebApp webapp = new WebApp();

            webapp.WebAppId = model.WebAppId;
         
            webapp.WebAppName = model.WebAppName;
            webapp.WebAppUrl = model.WebAppUrl;
            webapp.WebAppIp = model.WebAppIp;
           
            IWebAppservice webAppservice = new WebAppService(new WebAppRepository());
            var webappresponse = webAppservice.SaveData(webapp);
            Moniter moniter = new Moniter();
            
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString)) { 
                    try


                {


                    Ping myping = new Ping();
                    PingReply rep = myping.Send(webapp.WebAppIp, 1000);


                    if (rep.Status != IPStatus.Success)
                    {
                        model.MoniterServer = 0;
                    }
                    else
                    {
                        model.MoniterServer = 1;
                    }



                    var url = webapp.WebAppUrl;
                    var req = (HttpWebRequest)WebRequest.Create(url);
                    var res = (HttpWebResponse)req.GetResponse();
                    if (res.Headers["Db"] == null)
                    {
                        model.MoniterDb = 1;
                    }
                    else
                    {
                        model.MoniterDb = 0;
                    }
                    model.MoniterApp = 1;
                    var a = model.WebAppName;
                }
                catch (Exception e)
                {
                    model.MoniterApp = 0;

                }
                    string sqlText = "INSERT INTO [dbo].[Moniter] ([CreateDate],[MoniterTime],[WebAppId],[MoniterServer],[MoniterApp],[MoniterDb]) " +
                                     "VALUES (getdate(),Convert(Time, GetDate())," + webapp.WebAppId + "," +model.MoniterServer + "," + model.MoniterApp + "," + model.MoniterDb + ") " +
                                     "SELECT MoniterId = CONVERT(int, SCOPE_IDENTITY());";
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));
                    model.MoniterId = (int)dataSet.Tables[0].Rows[0][0];

                
       

            }



         

            return RedirectToAction(MoniterController.ActionList);
        }


        public IActionResult Delete(int? editid, string name)

        {

            IWebAppservice service2 = new WebAppService(new WebAppRepository());


            IMoniterService service = new MoniterService(new MoniterRepository());



         
            service.DeleteData((int) editid);
                service2.DeleteData((int)editid);



            return RedirectToAction(MoniterController.ActionList);
        }



    }
   



    }

