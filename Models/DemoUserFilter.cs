﻿namespace  MonitorApp
{
    public class DemoUserFilter
    {
        public string Keyword { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public bool? IsActive { get; set; }
    }
}
