﻿using System;
using System.Globalization;

namespace  MonitorApp
{
    public class BaseModel
    {
        protected string FormatDate(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("dd/MM/yyyy", CultureInfo.GetCultureInfo("en-US")) : null;
        }

        protected string FormatDateTime(DateTime? value)
        {
            return value.HasValue ? value.Value.ToString("dd/MM/yyyy HH:mm", CultureInfo.GetCultureInfo("en-US")) : null;
        }
    }
}
