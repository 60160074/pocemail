﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class ListMoniterModel : BaseListModel
    {
        public ListMoniterModel()
        {
            Moniters = new List<MoniterModel>();
        }
        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public string WebAppIp { get; set; }
        public int pageNo { get; set; }
        public int CountError { get; set; }
        public int CountAvailble { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public TimeSpan MoniterTime { get; set; }

        public string Keyword { get; set; }
        public List<MoniterModel> Moniters { get; protected set; }
        public SelectList NameList { get; protected set; }
        public ResultPaging ToMoniterPaging()
        {
            return new ResultPaging(UCHelper.PagePerScreen, pageNo);
        }
        public override object ToPagingParameter(int pageNo)
        {
            return new { moniterId = MoniterId, moniterServer = MoniterServer, pageNo = pageNo, monitorApp = MoniterApp ,webappName = WebAppName, webappUrl= WebAppUrl,webappIp=WebAppIp, createDate = CreateDate , enddate = EndDate };
        }
        public MoniterFilter ToMonitorFilter()
        
        {
            return new MoniterFilter()
            {
                WebAppName = WebAppName,
                WebAppId = WebAppId,
                WebAppIp = WebAppIp,
                WebAppUrl = WebAppUrl,
                CreateDate = CreateDate,
                EndDate = EndDate,

            };
        }
        public void FillLookupList()
        {
        NameList = new SelectList(Moniters, "WebAppId", "WebAppName");
       

        }
    }
}
