﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace  MonitorApp
{
    public class MonitorModel : BaseEditModel
    {
        public int? MoniterId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb{ get; set; }
        public int CreateUserId { get; set; }
        public int WebAppId { get; set; }
        public string WebAppIp { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EndDate { get; set; }
        public TimeSpan MoniterTime { get; set; }
    }
}
