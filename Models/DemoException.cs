﻿using System;

namespace  MonitorApp
{
    public class DemoException : ApplicationException
    {
        public DemoException(BizError error, int? id = null)
            : base()
        {
            Error = error;
            Id = id;
        }

        public BizError Error { get; protected set; }
        public int? Id { get; set; }
    }
}
