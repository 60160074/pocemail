﻿using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    public class DemoConfigSection : CentralConfigurationSection
    {
        public static readonly string SectionName = "Demo";

        public static DemoConfigSection GetInstance()
        {
            return (DemoConfigSection)CentralConfigurationManager.GetSection(SectionName, typeof(DemoConfigSection));
        }

        public static void Save(DemoConfigSection section)
        {
            CentralConfigurationManager.SaveSection(SectionName, section);
        }

        [CentralConfigurationProperty("ItemPerPage", DataType.Int, DefaultValue = 20)]
        public int ItemPerPage
        {
            get { return (int)this["ItemPerPage"]; }
            set { this["ItemPerPage"] = value; }
        }

        [CentralConfigurationProperty("ItemPerDialog", DataType.Int, DefaultValue = 10)]
        public int ItemPerDialog
        {
            get { return (int)this["ItemPerDialog"]; }
            set { this["ItemPerDialog"] = value; }
        }
    }
}
