﻿
using System;
using System.Collections.Generic;

namespace  MonitorApp
{
    public class MoniterFilter
    {

        public int? MonitorId { get; set; }
        public int MoniterServer { get; set; }
        public int MoniterApp { get; set; }
        public int MoniterDb { get; set; }
        public int CreateUserId { get; set; }
        public int? WebAppId { get; set; }
        public string WebAppIp { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EndDate { get; set; }

        public TimeSpan MoniterTime { get; set; }
        public string WebAppName { get; set; }
        public string WebAppUrl { get; set; }
    }
}
