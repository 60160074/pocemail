﻿using MonitorApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TwoMind.Infrastructure.Entity.Sql;
using TwoMind.Infrastructure.Model;
using System.Transactions;
using System.Net;
using System.Net.NetworkInformation;

namespace MonitorApp
{
    public class MoniterRepository : BaseRepository, IMoniterRepository
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(DemoRoleRepository));

        public List<Moniter> GetList(MoniterFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                if (filter.WebAppId == 0)
                {
                    filter.WebAppId = null;
                }

                string sqlSelect = "select *";
                string sqlFromWhere = "from (select w.WebAppId,w.WebAppName,w.WebAppUrl,w.WebAppIp,w.CreateUserId,w.ModifyUserId,w.ModifyDate,m.MoniterId,m.MoniterServer,m.MoniterApp,m.MoniterDb,m.CreateDate,[MoniterTime],ROW_NUMBER() OVER(PARTITION BY w.WebAppId ORDER BY MoniterTime Desc) as t,ROW_NUMBER() OVER(PARTITION BY w.WebAppId ORDER BY m.CreateDate Desc)as d from WebApp w LEFT JOIN Moniter m on m.WebAppId = w.WebAppId) a where  d = 1  " + " And (@webappId IS NULL OR WebAppId = @webappId)" + " And (@webappUrl IS NULL OR WebAppUrl = @webappUrl)" + " And (@webappIp IS NULL OR WebAppIp = @webappIp)"
                    + " And (@createDate IS NULL OR CreateDate >= @createDate)"
                    + " And (@endDate IS NULL OR CreateDate <= @endDate)"
                    + "And (@webappName IS NULL OR WebAppName = @webappName)";
                string sqlOrderBy = "ORDER BY WebappId";
                SqlParameter[] parameters = new SqlParameter[]
                   {  new SqlParameter("@endDate", filter == null || filter.EndDate == null ? (object)DBNull.Value : filter.EndDate),
                        new SqlParameter("@createDate", filter == null || filter.CreateDate == null ? (object)DBNull.Value : filter.CreateDate),
                        new SqlParameter("@webappId", filter == null || ! filter.WebAppId.HasValue ? (object)DBNull.Value : filter.WebAppId),
                        new SqlParameter("@webappIp", filter == null || filter.WebAppIp == null ? (object)DBNull.Value : filter.WebAppIp),
                         new SqlParameter("@webappName", filter.WebAppName == null || filter.WebAppName== null ? (object)DBNull.Value : filter.WebAppName),
                        new SqlParameter("@webappUrl", filter.WebAppUrl == null || filter.WebAppUrl== null ? (object)DBNull.Value : filter.WebAppUrl)
                   };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<Moniter> list = new List<Moniter>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new Moniter(), row));





                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }


        public List<Moniter> GetListLog(MoniterFilter filter, ResultPaging paging, ListMoniterModel model)
        {


            const string func = "GetLog";
            try
            {

                var name = model.WebAppName;
                string sqlSelect = "select WebApp.WebAppId,WebApp.WebAppName,WebApp.WebAppIp,WebApp.WebAppUrl,m.MoniterId,m.MoniterApp,m.MoniterServer,m.MoniterDb,m.MoniterTime,m.CreateDate";

                string sqlFromWhere = "from Moniter m left join WebApp on Webapp.WebAppId  = m.WebAppId" + " Where (@webappName IS NULL OR WebAppName = @webappName)" 
                                        + " And (@webappUrl IS NULL OR WebAppUrl = @webappUrl)" + " And (@webappIp IS NULL OR WebAppIp = @webappIp)"
                                        + " And (@createDate IS NULL OR m.CreateDate >= convert(varchar, @createDate, 1) )"
                                        + " And (@endDate IS NULL OR m.CreateDate <= convert(varchar, @endDate, 1) )";

                string sqlOrderBy = "order by m.CreateDate desc";
                SqlParameter[] parameters = new SqlParameter[]
                    {
                        new SqlParameter("@endDate", filter == null || filter.EndDate == null ? (object)DBNull.Value : filter.EndDate),
                        new SqlParameter("@createDate", filter == null || filter.CreateDate == null ? (object)DBNull.Value : filter.CreateDate),


                        new SqlParameter("@webappName", filter.WebAppName == null || filter.WebAppName== null ? (object)DBNull.Value : filter.WebAppName),
                        new SqlParameter("@webappIp", filter == null || filter.WebAppIp == null ? (object)DBNull.Value : filter.WebAppIp),
                        new SqlParameter("@webappUrl", filter.WebAppUrl == null || filter.WebAppUrl== null ? (object)DBNull.Value : filter.WebAppUrl)

                    };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<Moniter> list = new List<Moniter>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new Moniter(), row));





                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public List<Moniter> Moniter(List<Moniter> list, Moniter moniter)
        {
            const string func = "Monitor";
            try
            {


                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();






                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }
        public Moniter GetData(int IdItem)

        {
            const string func = "GetData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection,
                       "Select *" +
                       " from (select w.WebAppId,w.WebAppName,w.WebAppUrl,w.WebAppIp,w.CreateUserId,w.ModifyUserId,w.ModifyDate,m.MoniterId,m.MoniterServer,m.MoniterApp,m.MoniterDb,m.CreateDate,[MoniterTime],ROW_NUMBER() OVER(PARTITION BY w.WebAppId ORDER BY MoniterTime Desc) as t,ROW_NUMBER() OVER(PARTITION BY w.WebAppId ORDER BY m.CreateDate Desc)as d from WebApp w LEFT JOIN Moniter m on m.WebAppId = w.WebAppId) a " +
                        "WHERE WebAppId =" + IdItem + "and d = 1; ",
                        new SqlParameter("@areaId", IdItem));
                    if (dataSet.Tables[0].Rows.Count != 1)
                        return null;
                    return DataHelper.FillDataModel(new Moniter(), dataSet.Tables[0].Rows[0]);
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with user id {1}.", func, IdItem, ex);
                throw ex;
            }
        }



        public virtual Moniter SaveData(Moniter moniter)
        {
            const string func = "SaveData";
            try
            {
                if (moniter == null)
                    throw new ArgumentException("Moniter is null.");
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    if (!moniter.MoniterId.HasValue)
                    {
                        string sqlText = "INSERT INTO [Moniter] (MoniterServer,MoniterApp,MoniterDb,CreateUserId,WebAppId,ModifyDate,ModifyTime) " +
                                     "VALUES (@MoniterServer,@moniterApp,@moniterDb,1,@webAppId,GETDATE(), convert(time(0),getDate())); " +
                                     "SELECT MoniterId = CONVERT(int, SCOPE_IDENTITY());";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));
                        moniter.MoniterId = (int)dataSet.Tables[0].Rows[0][0];
                    }
                    else
                    {


                        string sqlText = "UPDATE [TestArea] SET Name = @name, " +
                                   "   ProvinceId = @provinceId, " +
                                   "   ZoneId = @zoneId, " +
                                   "   ModifyDate = GETDATE(), " +

                                   "WHERE AreaId = @areaId";
                        DataHelper.ExecuteNonQuery(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, moniter));
                    }
                }
                return moniter;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }





        public virtual bool DeleteData(int moniterId)
        {
            const string func = "DeleteData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataHelper.ExecuteNonQuery(connection,
                        "DELETE FROM [Moniter] WHERE WebAppId = @moniterId;",


                        new SqlParameter("@moniterId", moniterId));
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with lost id {1}.", func, moniterId, ex);
                throw ex;
            }
        }
    }
}






