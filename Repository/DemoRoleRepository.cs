﻿using  MonitorApp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TwoMind.Infrastructure.Entity.Sql;
using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    public class DemoRoleRepository : BaseRepository, IDemoRoleRepository
    {
        private static readonly Logger _logger = LoggerFactory.GetLogger(typeof(DemoRoleRepository));

        public virtual List<DemoRole> GetList(DemoRoleFilter filter, ResultPaging paging)
        {
            const string func = "GetList";
            try
            {
                string sqlSelect = "SELECT RoleId, OrderNo, Name, IsActive, IsSystem";
                string sqlFromWhere = "FROM Role " +
                    "WHERE (@name IS NULL OR LOWER(Name) LIKE LOWER(@name)) " +
                    "   AND (@isActive IS NULL OR IsActive = @isActive) " +
                    "   AND (@isSystem IS NULL OR IsSystem = @isSystem)";
                string sqlOrderBy = "ORDER BY Name";
                SqlParameter[] parameters = new SqlParameter[]
                    {
                        new SqlParameter("@name", filter == null || filter.Name == null ? (object)DBNull.Value : filter.Name),
                        new SqlParameter("@isActive", filter == null || !filter.IsActive.HasValue ? (object)DBNull.Value : filter.IsActive),
                        new SqlParameter("@isSystem", filter == null || !filter.IsSystem.HasValue ? (object)DBNull.Value : filter.IsSystem)
                    };
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet;
                    if (paging == null)
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildNonPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy), parameters);
                    else
                    {
                        dataSet = DataHelper.ExecuteDataSet(connection, DataHelper.BuildPaginationQuery(sqlSelect, sqlFromWhere, sqlOrderBy, paging), parameters);
                        paging.TotalItem = (int)dataSet.Tables[1].Rows[0][0];
                    }
                    List<DemoRole> list = new List<DemoRole>();
                    foreach (DataRow row in dataSet.Tables[0].Rows)
                        list.Add(DataHelper.FillDataModel(new DemoRole(), row));
                    return list;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual DemoRole GetData(int roleId)
        {
            const string func = "GetData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataSet dataSet = DataHelper.ExecuteDataSet(connection,
                        "SELECT RoleId, OrderNo, Name, Note, IsActive, IsSystem " +
                        "FROM Role " +
                        "WHERE RoleId = @roleId",
                        new SqlParameter("@roleId", roleId));
                    if (dataSet.Tables[0].Rows.Count != 1)
                        return null;
                    DemoRole role = DataHelper.FillDataModel(new DemoRole(), dataSet.Tables[0].Rows[0]);
                    return role;
                }
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with role id {1}.", func, roleId, ex);
                throw ex;
            }
        }

        public virtual DemoRole SaveData(DemoRole role)
        {
            const string func = "SaveData";
            try
            {
                if (role == null)
                    throw new ArgumentException("Role is null.");
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    if (!role.RoleId.HasValue)
                    {
                        string sqlText = "INSERT INTO Role (OrderNo, Name, Note, IsActive, IsSystem) " +
                            "VALUES (@orderNo, @name, @note, @isActive, 0); " +
                            "SELECT [Id] = CONVERT(int, SCOPE_IDENTITY());";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, role));
                        role.RoleId = (int)dataSet.Tables[0].Rows[0][0];
                    }
                    else
                    {
                        string sqlText = "UPDATE Role SET OrderNo = @orderNo, " +
                            "   Name = @name, " +
                            "   Note = @note, " +
                            "   IsActive = @isActive " +
                            "WHERE RoleId = @roleId";
                        DataSet dataSet = DataHelper.ExecuteDataSet(connection, sqlText, DataHelper.BuildSqlParameters(sqlText, role));
                    }
                }
                return role;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught.", func, ex);
                throw ex;
            }
        }

        public virtual bool DeleteData(int roleId)
        {
            const string func = "DeleteData";
            try
            {
                using (SqlConnection connection = new SqlConnection(EntityConfigSection.ConnectionString))
                {
                    connection.Open();
                    DataHelper.ExecuteNonQuery(connection,
                        "DELETE FROM Role WHERE RoleId = @roleId AND IsSystem = 0",
                        new SqlParameter("@roleId", roleId));
                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("{0}: Exception caught with role id {1}.", func, roleId, ex);
                throw ex;
            }
        }
    }
}
