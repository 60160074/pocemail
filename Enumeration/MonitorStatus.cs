﻿namespace  MonitorApp
{
    public enum MonitorStatus
    {
        AppDown = 1,
        ServerDown = 2,
        DatabaseDown = 3,
        Avaliable = 4,
    }
}
