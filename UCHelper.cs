﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;
using TwoMind.Infrastructure.WebUI;
//using Cpd.Fcs.Web.Resources;

namespace MonitorApp
{
    public static class UCHelper
    {
        public static readonly int PagePerScreen = 10;
        public static readonly int PagePerDialog = 3;

        public static string Pager(IUrlHelper url, IPagingable paginable, string actionName)
        {
            return Pager(url, paginable, actionName, PagePerScreen);
        }

        public static string DialogPager(IUrlHelper url, IPagingable paginable, string actionName)
        {
            return Pager(url, paginable, actionName, PagePerDialog);
        }

        private static string Pager(IUrlHelper url, IPagingable pagingable, string actionName, int pagePerScreen)
        {
            int pageNo = pagingable.Paging.PageNo > 0 ? pagingable.Paging.PageNo : 1;
            int totalPage = pagingable.Paging.TotalPage > 0 ? pagingable.Paging.TotalPage : 1;
            int beginPageNo = pageNo - (pageNo % pagePerScreen == 0 ? pagePerScreen : pageNo % pagePerScreen) + 1;
            int endPageNo = beginPageNo + pagePerScreen - 1;
            if (endPageNo > totalPage)
                endPageNo = totalPage;

            PagingModel p = pagingable.Paging;
            int x = p.FirstItemNo > p.TotalItem ? 0 : p.FirstItemNo;
            int y = p.PageNo >= p.TotalPage ? p.TotalItem : x + p.ItemPerPage - 1;
            int z = p.TotalItem;

            StringBuilder sb = new StringBuilder();
            //sb.Append("<div class='wa-pagging'>");
            ////sb.AppendFormat("<label>แสดงข้อมูล {0} - {1} จาก {2} รายการ</label>", x, y, z);
            //sb.Append("<label>" + string.Format(Resource.PagingInfo, x, y, z) + "</label>");
            sb.Append("<nav>");
            sb.Append("<ul class='pagination mb-0'>");
            sb.AppendFormat("<li class='page-item {0}'><a class='page-link' href='{1}'>&lsaquo;</a></li>",
            pageNo > 1 ? "" : "disabled",
            pageNo > 1 ? url.Action(actionName, pagingable.ToPagingParameter((pageNo - 1) > 1 ? pageNo - 1 : 1)) : "#");

            for (int i = beginPageNo; i <= endPageNo; i++)
            {
                var paging = pagingable.ToPagingParameter(i);
                sb.AppendFormat("<li class='page-item {0}'><a class='page-link' href='{1}'>{2}</a></li>",
                i == pageNo ? "active" : "",
                url.Action(actionName, pagingable.ToPagingParameter(i)),
                i);
            }

            sb.AppendFormat("<li class='page-item {0}'><a class='page-link' href='{1}'>&rsaquo;</a></li>",
            pageNo < totalPage ? "" : "disabled",
            pageNo < totalPage ? url.Action(actionName, pagingable.ToPagingParameter(pageNo + 1 < totalPage ? pageNo + 1 : totalPage)) : "#");
            sb.Append("</ul>");
            sb.Append("</nav>");
            //sb.Append("</div>");
            return sb.ToString();
        }

        public static string PageInfo(IPagingable pagingable)
        {
            PagingModel p = pagingable.Paging;
            int x = p.FirstItemNo > p.TotalItem ? 0 : p.FirstItemNo;
            int y = p.PageNo >= p.TotalPage ? p.TotalItem : x + p.ItemPerPage - 1;
            int z = p.TotalItem;
            return string.Format("แสดงข้อมูล " + x + " - " + y + " จาก " + z + " รายการ");
        }

        public static string FcsPaging(IUrlHelper url, IPagingable pagingable, string actionName)
        {
            return FcsPaging(url, pagingable, actionName, PagePerScreen);
        }

        public static string FcsPaging(IUrlHelper url, IPagingable pagingable, string actionName, int pagePerScreen)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class=\"d-inline-flex w-100 justify-content-between align-self-center paging\">");
            sb.Append(PageInfo(pagingable));
            sb.Append("<label>" + Pager(url, pagingable, actionName, pagePerScreen) + "</label>");
            sb.Append("</div>");
            return sb.ToString();
        }
        public static string DropDownList(string name, IEnumerable<SelectListItem> selectList, string selectedValue, string htmlAttributes)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<select name='{0}' id='{0}' {1}>", name, htmlAttributes);
            foreach (SelectListItem item in selectList)
            {
                sb.AppendFormat("<option value='{0}' {1}>{2}</option>", item.Value, String.Equals(item.Value, selectedValue) ? "selected" : "", item.Text);
            }
            sb.Append("</select>");
            return sb.ToString();
        }
    }
}