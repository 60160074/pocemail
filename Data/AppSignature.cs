﻿using System.Collections.Generic;

namespace  MonitorApp
{
    public static class AppSignature
    {
        private static Dictionary<AppSecurable, string> _signatures = new Dictionary<AppSecurable, string>()
        {
        };

        public static string GetSecurableSignature(AppSecurable appSecurable)
        {
            return _signatures.ContainsKey(appSecurable) ? _signatures[appSecurable] : null;
        }
    }
}
