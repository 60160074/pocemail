﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using TwoMind.Infrastructure.Model;

namespace  MonitorApp
{
    [Serializable]
    public class AppSession
    {
        public AppSession()
        {
            Signatures = new List<string>();
        }

        public static AppSession CreateInstance(ISession session)
        {
            string value = session.GetString(SessionName);
            return value == null ? null : JsonConvert.DeserializeObject<AppSession>(value);
        }

        public static readonly string SessionName = "Session";
        public static readonly string CultureName = "Culture";
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public List<string> Signatures { get; protected set; }

        public List<Securable> Securables
        {
            set
            {
                Signatures.Clear();
                if (value != null)
                    GetSignatures(value, Signatures);
            }
        }

        private void GetSignatures(List<Securable> securables, List<string> signatures)
        {
            foreach (Securable securable in securables)
            {
                signatures.Add(securable.Signature);
                GetSignatures(securable.ChildSecurables, signatures);
            }
        }

        public bool IsAuthorized(AppSecurable appSecurable)
        {
            string signature = AppSignature.GetSecurableSignature(appSecurable);
            if (signature == null)
                return false;
            return Signatures.Contains(signature);
        }

        public bool IsAuthorized(List<Securable> securables, AppSecurable appSecurable)
        {
            string signature = AppSignature.GetSecurableSignature(appSecurable);
            if (signature == null)
                return false;
            return IsAuthorized(securables, signature);
        }

        private bool IsAuthorized(List<Securable> securables, string signature)
        {
            foreach (Securable securable in securables)
            {
                if (securable.Signature == signature || IsAuthorized(securable.ChildSecurables, signature))
                    return true;
            }
            return false;
        }
    }
}
