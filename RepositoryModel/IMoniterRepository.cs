﻿using System.Collections.Generic;

using TwoMind.Infrastructure.Model;

namespace MonitorApp
{
    public interface IMoniterRepository
    {
        
        List<Moniter> GetListLog(MoniterFilter filter, ResultPaging paging,ListMoniterModel moniter);
        List<Moniter> GetList(MoniterFilter filter, ResultPaging paging);
        List<Moniter> Moniter(List<Moniter> webApp,Moniter moniter);
        Moniter GetData(int moniterId);
        Moniter SaveData(Moniter moniter);
        bool DeleteData(int moniterid);
    }
}
